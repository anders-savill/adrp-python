import docker
import math
import custom_settings as cs
#Configure the Docker Server details
class dockerServer:
    def __init__(self, certpath, serverip, serverport, clientcert, clientkey, cacert, otimeout=10):
        tls_config = docker.tls.TLSConfig(client_cert=(f'{certpath}/{clientcert}' , f'{certpath}/{clientkey}'), ca_cert=(f'{certpath}/{cacert}'), verify=True)
        self.client = docker.DockerClient(base_url=f'https://{serverip}:{serverport}', tls=tls_config, timeout=otimeout)

class servers():
    try:
        #If there is a local instance of docker running on the machine this will try and connect to it
        #If you run this in a container you need to bind mount /var/run into the container. Do not bind /var/run/docker as this will only work for the life of the socket
        dockerlocal = docker.from_env()
        dockerlocal.info()
        print(f'Dockerlocal is up')
    except:
        print(f'Failed to connect to Dockerlocal')

    serverConfiguration = cs.serverConfiguration
    #Here we are loading custom_settings.py
    #Now we will take those settings and attempt to connect to each server in order
    for name, config in serverConfiguration.items():
        try:
            locals()[name] = dockerServer(
            certpath = config['certpath'],
            serverip = config['serverip'],
            serverport = config['serverport'],
            clientcert = config['clientcert'],
            clientkey = config['clientkey'],
            cacert = config['cacert']).client
            svInfo = locals()[name].info()
            ctot = svInfo['Containers']
            crun = svInfo['ContainersRunning']
            print(f'{name} is up with {crun}/{ctot} containers running')
        except:
            print(f'Failed to connect to {name}')

#Specify the default client. We don't explicitly require the user to specify the connection so we need a default
defaultclient = servers.dockerclient2

def getContainerInfo(dockerclient=defaultclient):
    #This will get all the same kind of information that docker ps -a does and return it as a list or except
    try:
        nodesOut = {}
        nodes = dockerclient.containers.list(all=True)
        for n in nodes:
            nodesOut[n.name] = {'name': n.name, 'short_id': n.short_id, 'id': n.id, 'status': n.status}
            #Build a dict with all active and non active containers
    except:
        raise Exception('Failed')
        nodesOut = ''
    return nodesOut

def getImageInfo(dockerclient=defaultclient):
    #This should return all of the images currently loaded on the server
    nodes = dockerclient.images.list(all=True)
    return nodes

def runContainer(imageName, imageVersion='latest', runArgs='', directories='', commands='', contName='', dockerclient=defaultclient):
    try:
        runTainer = dockerclient.containers.run(image=f'{imageName}:{imageVersion}', command=commands, entrypoint=['/bin/bash','-c'], name=contName, volumes=directories, stdin_open=True, stdout=True, stderr=True, detach=True)
        #This is the code that will start the container with the default configuration, you just need to specify the image name and also commands to run in the container based on the docker specification
        response = runTainer.logs()
        #Convert the logs to a string and strip the trailing "'"
        container_id = runTainer.id
        #response = str(response).rstrip("'")
        containerinf = {'success':True, 'message':container_id}
    except docker.errors.APIError as response:
        containerinf = {'success':False, 'message':str(response)}
        #We try and return dicts for everything so it's easy to parse into django
    return containerinf

def serverStatus(dockerclient=defaultclient):
    #Provide the status of a specific server and regular information about it
    svstatus = dockerclient.info()
    svstatus['MemTotal'] = round((svstatus['MemTotal'] / 1024 / 1024 / 1024),2)
    return svstatus

def allServerStatus(dockerclient=defaultclient):
    #Same as serverStatus except it will go through all of the active servers and retrieve their status
    dictOut = {}
    for x in dir(servers):
        if type(getattr(servers, x)) == type(docker.from_env()):
            try:
                svInfo = getattr(servers, x).info()
                ctot = svInfo['Containers']
                crun = svInfo['ContainersRunning']
                cname = svInfo['Name']
                dictOut[x] = {'Name': cname, 'Up': True, 'ContainersRunning':crun, 'Containers':ctot}
            except:
                dictOut[x] = {'Name': x, 'Up': False}
    return dictOut

def containerStatus(containerID, dockerclient=defaultclient):
    #get the status of a specific container
    try:
        requested_container = dockerclient.containers.get(containerID)
    except:
        requested_container = 'Unable to get container object. The container may no longer exist.'
    return requested_container

def containerPrune(dockerclient=defaultclient):
    #This will prune all non running containers
    try:
        response = dockerclient.containers.prune()
    except docker.errors.APIError as Eresponse:
        response = {'success': False, 'message': Eresponse}
    return response
