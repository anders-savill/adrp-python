import csv
import subprocess

configfile = open('reduce.csv')
commandString = []
for x in csv.DictReader(configfile):
    #step, obs_type, filepath, task, idxfile, args
    #We don't really care about step
    step = x['step']
    print(f'Beginning step {step}')
    #define IDX Location
    idxLocation = x['idxfile']
    dsplit = x['filepath'].rsplit(sep='/', maxsplit=1)
    directory = dsplit[0]
    print(dsplit)
    filename = dsplit[1].rsplit(sep='.', maxsplit=1)[0]
    extension = dsplit[1].rsplit(sep='.', maxsplit=1)[1]
    #I know this can be done better but I'm going to keep it as if/elif statements because it makes it easier to read and manage imo
    if x['task'] == 'maketlm':
        result = subprocess.run(args = [f'aaorun reduce_fflat {directory}/{filename}.{extension} -idxfile {idxLocation} -OUT_DIRNAME . -SKYSCRUNCH 0 -USEBIASIM 0 -USEDARKIM 0 -USEBIASIM 0 -USEDARKIM 0 -USEFLATIM 0'], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        tlm = f'{filename}tlm.fits'
        print(f'Making Tramline {result.stdout}')

    elif x['task'] == 'makewavel':
        arcLocation = x['filepath']
        result = subprocess.run(args = [f'aaorun reduce_arc {directory}/{filename}.{extension} -idxfile {idxLocation} -OUT_DIRNAME . -SKYSCRUNCH 0 -USEBIASIM 0 -USEDARKIM 0 -USEBIASIM 0 -USEDARKIM 0 -EXTR_OPERATION GAUSS -USEFLATIM 0 -TLMAP_FILENAME {tlm}'], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        wavel = f'{filename}red.fits'
        print(f'Making Wavelength {result.stdout}')

    elif x['task'] == 'reduceflat':
        result = subprocess.run(args = [f'aaorun reduce_fflat {directory}/{filename}.{extension} -idxfile {idxLocation} ' \
            f'-OUT_DIRNAME . -SKYSCRUNCH 0 -USEBIASIM 0 -USEDARKIM 0 -USEBIASIM 0 -USEDARKIM 0 -USEFLATIM 0 -WAVEL_FILENAME {wavel}'], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        fflat = f'{filename}red.fits'
        print(f'Reducing Flat {result.stdout}')

    elif x['task'] == 'reduceobject':
        result = subprocess.run(args = [f'ls -l; aaorun reduce_object {directory}/{filename}.{extension} -idxfile {idxLocation} ' \
         f' -OUT_DIRNAME . -SKYSCRUNCH 1 -USEBIASIM 0 -USEDARKIM 0 -USEBIASIM 0 -USEDARKIM 0 -TPMETH "SKYFLUX(MED)" -USEFLATIM 0 -TLMAP_FILENAME {tlm} ' \
          f'-WAVEL_FILENAME {wavel} -FFLAT_FILENAME {fflat}'], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        print(f'Reducing Object {result.stdout}')
    else:
        print('Unknown Command')
