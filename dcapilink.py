import docker_manage as dk
import command_library as cmdx

def aaoReduce(uID, folderLocation, workflowID):
    #uID must be unique or container creation will fail. uID is a uID you supply and only one running container can have the same uID
    #We can expand this later if we want to specify additional directories and/or change the output directory
    #This is the same as the docker run -v (volume binds command)
    directories = [f'{folderLocation}:/astrodata:ro', '/misc/astrofs/astroout:/astro_out:rw']
    #We want to copy the files from the input directory to be worked on because we want to be sure we don't mess any of the input files up
    aao_commands = cmdx.commandLibrary[workflowID]
    #Instead of speicfying a command string passed from django or elsewhere it seemed a bit more secure to pre-write the commands and then call them here
    #There's no reason you can't change the aao_commands variable to a different string
    commands = [f'cd /astrodata/;{aao_commands}']
    reduceContainer = dk.runContainer(dockerclient=dk.servers.dockerclient2, imageName='anderssavill/basic', contName=uID, directories=directories, commands=commands)
    return reduceContainer

#This is a function for testing aaoReduce
def TEST_aaoReduce():

    run_output = aaoReduce('12345', '/misc/astrofs/astrodata/user1', 'reduce_full')
    print(run_output)

def aaoStatus(uID):
    fullContainer = dk.containerStatus(containerID=uID)
    #When starting a container with aaoReduce record the uID as this will be passed to aaoStatus
    containerInfo = {}
    try:
        fullContainer.logs()
        containerLogOut = fullContainer.logs(stdout=True, stderr=False, stream=False).decode('UTF-8')
        containerLogErr = fullContainer.logs(stdout=False, stderr=True, stream=False).decode('UTF-8')
        if(containerLogErr != ''):
            containerStatus = 'Failed'
        else:
            containerStatus = fullContainer.status
        containerInfo = {'status': containerStatus, 'logErr':containerLogErr, 'logOut':containerLogOut}
        #If you print out containerLog in Django you have to put it in <pre> tags
    except:
        containerInfo = {'status':'Error', 'logErr':fullContainer, 'logOut':'Error'}
    return(containerInfo)

#Function for testing aaoStatus
def TEST_aaoStatus():
    result = aaoStatus('12345')
    cs = result['status']
    print(f'Container status: {cs}')
    if(cs == 'Failed' or cs =='Error'):
        rs = result['logErr']
        print(f'Internal Container Error: {rs}')

def MAINT_aaoPrune():
    print(dk.containerPrune())
