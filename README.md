adrp-python

You can use this directly on a docker host and it will connect to your docker socket.
If you want to connect to external services you need to configure the docker hosts to use TLS and acquire the certificates https://docs.docker.com/engine/security/https/
