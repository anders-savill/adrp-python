import csv
''' Command Configuration
Each workflow gets put in a dict, this could be loaded from anywhere at a later date, even from a django DB
'''
def reducegen(configfile="depreciated/reduce.csv"):
    configfile = open(configfile)
    commandString = []
    for x in csv.DictReader(configfile):
        #step, obs_type, filepath, task, idxfile, args
        #We don't really care about step
        step = x['step']
        print(f'Beginning step {step}')
        if x['task'] == 'maketlm':
            flatLocation = x['filepath']
            idxLocation = x['idxfile']
            commandString.append(f'aaorun reduce_fflat {flatLocation} -idxfile {idxLocation} -OUT_DIRNAME . -SKYSCRUNCH 0 -USEBIASIM 0 -USEDARKIM 0 -USEBIASIM 0 -USEDARKIM 0 -USEFLATIM 0;')

        elif x['task'] == 'makewavel':
            flatLocation = x['filepath']
            idxLocation = x['idxfile']
            arcLocation = x['filepath']
            commandString.append(f'aaorun reduce_arc {arcLocation} -idxfile {idxLocation} -OUT_DIRNAME . -SKYSCRUNCH 0 -USEBIASIM 0 -USEDARKIM 0 -USEBIASIM 0 -USEDARKIM 0 -EXTR_OPERATION GAUSS -USEFLATIM 0 -TLMAP_FILENAME 14apr20026tlm.fits;')

        elif x['task'] == 'reduceflat':
            flatLocation = x['filepath']
            idxLocation = x['idxfile']
            arcLocation = x['filepath']
            commandString.append(f'aaorun reduce_fflat {arcLocation} -idxfile {idxLocation} -OUT_DIRNAME . -SKYSCRUNCH 0 -USEBIASIM 0 -USEDARKIM 0 -USEBIASIM 0 -USEDARKIM 0 -USEFLATIM 0 -WAVEL_FILENAME 14apr20025red.fits;')

        elif x['task'] == 'reduceobject':
            flatLocation = x['filepath']
            idxLocation = x['idxfile']
            commandString.append(f'aaorun reduce_object {arcLocation} -idxfile {idxLocation} -OUT_DIRNAME . -SKYSCRUNCH 1 -USEBIASIM 0 -USEDARKIM 0 -USEBIASIM 0 -USEDARKIM 0 -TPMETH "SKYFLUX(MED)" -USEFLATIM 0 -TLMAP_FILENAME 14apr20026tlm.fits -WAVEL_FILENAME 14apr20025red.fits -FFLAT_FILENAME 14apr20026red.fits;')
        else:
            print('Unknown Command')
    return commandString

commandLibrary = {}

#This is the AAO test
aao_workdir = '/tmp/working'
commandLibrary['example_data_process'] = 'aaorun list;' \
    f'fitsheader --extension 0 --keyword "OBSTYPE" *.fits -t csv;' \
    f'mkdir {aao_workdir};' \
    f'cp * {aao_workdir};' \
    f'cd {aao_workdir};' \
    f'echo "Running aaorun now";' \
    f'aaorun reduce_run 14apr2 -idxfile sami1000R.idx;'

commandLibrary['reduce_full'] = f'mkdir {aao_workdir};' \
    f'cp * {aao_workdir};' \
    f'cd {aao_workdir};' \
    f'echo "Files Copied to {aao_workdir}";' \
    f'echo "Running aaorun through python";' \
    f'python3.6 reduce.py;' \
    f'echo "Copy Files";' \
    f'cp -r {aao_workdir} /astro_out/;' \
    f'echo "Finished";'
